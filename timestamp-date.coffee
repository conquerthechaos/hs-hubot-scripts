# Description
#   <description of the scripts functionality>
#
# Dependencies:
#   "<module name>": "<module version>"
#
# Configuration:
#   LIST_OF_ENV_VARS_TO_SET
#
# Commands:
#   hubot <trigger> - <what the respond trigger does>
#   <trigger> - <what the hear trigger does>
#
# URLS:
#   GET /path?param=<val> - <what the request does>
#
# Notes:
#   <optional notes required for the script>
#
# Author:
#   <github username of the original script author>

module.exports = (robot) ->

  robot.respond /convert timestamp (.*)/i, (msg) ->
    time = msg.match[1]
    date = new Date(time * 1e3)
    diff = ((+new Date - date.getTime()) / 1e3)
    ddiff = Math.floor(diff / 86400)
    d = date.getDate()
    m = "Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec".split(" ")[date.getMonth()]
    y = date.getFullYear().toString()
    msg.reply("#{time} is #{m} #{d}, #{y} #{date.getHours()}:#{date.getMinutes()} (GMT-4)")
    