# Description:
#   Add dones and goals to idonethis.
#
# Dependencies:
#   None
#
# Configuration:
#   IDONETHIS_TEAM
#
# Commands:
#   done: <done> -- Add done
#   todo: <goal> -- Add goal
#
# Author:
#   viki

url  = 'https://idonethis.com/api/v0.1/'

team = process.env.IDONETHIS_TEAM
teams = process.env.IDONETHIS_TEAMS

member_success_flow_id = "057cd0b7-cf65-46be-b66d-6bcdb1498107"
tech_dev_flow_id       = "679cdbf6-e9ae-49d0-b000-811f00f9b5ce"
project_300_flow_id    = "00f6ce00-436d-4e44-9e17-176231d0cca7"

# team = "Tech. & Dev."

postRequest = (msg, path, api_key, params, callback) ->
  stringParams = JSON.stringify params
  auth = 'Token ' + new Buffer("#{api_key}").toString()
  msg.http("#{url}#{path}")
    .headers("Authorization": auth, "Accept": "application/json"
           , "Content-Length": stringParams.length, "Content-Type": "application/json")
    .post(stringParams) (err, res, body) ->
      callback(err, res, body)

getRequest = (msg, path, api_key, callback) ->
  auth = 'Token ' + new Buffer("#{api_key}").toString()
  msg.http("#{url}#{path}")
    .headers("Authorization": auth, "Accept": "application/json")
    .get() (err, res, body) ->
      callback(err, res, body)

addDone = (msg, api_key, params) ->
  postRequest msg, 'dones/', api_key, params, (err, res, body) ->

    try response = JSON.parse body 
    catch e then msg.send "You can’t post ‘done: ‘ here as you’re not a member of the iDoneThis ‘#{params.team}’ team. Sorry!" 
    finally
      if response.ok is true
        message_to_send = "Added to #{response.result.team_short_name} in iDoneThis for #{response.result.owner}"
        if(params.jira == "yes")
          message_to_send += " and moved issue to done in JIRA"
        msg.send message_to_send
      else
        msg.send "Error adding task: " + body + "."


addGoal = (msg, api_key, params) ->
  params.raw_text = '[] ' + params.raw_text
  postRequest msg, 'dones/', api_key, params, (err, res, body) ->
    response = JSON.parse body

    if response.ok is true
      msg.send "Added to #{response.result.team_short_name} for #{response.result.owner}"
    else
      msg.send "Error adding todo: " + body + "."


getOwner = (msg, api_key, callback) ->
  getRequest msg, 'noop/', api_key, (err, res, body) ->
    response = JSON.parse body
    return callback({success: true, owner: response.user})
    callback({success: false})


getDones = (msg, api_key, owner, callback) ->
  getRequest msg, 'dones/?done_date=today&owner=' + new Buffer("#{owner}").toString(), api_key, (err, res, body) ->
    response = JSON.parse body
    if response.count is 0
      msg.send "You have not got anything done today (sadpanda), there is still time to change that"
    else
      for done in response.results
        if done.goal_completed is true
          msg.send "#{done.raw_text}"




getGoals = (msg, api_key, owner, callback) ->
  getRequest msg, 'dones/?done_date=today&owner=' + new Buffer("#{owner}").toString(), api_key, (err, res, body) ->
    response = JSON.parse body
    if response.count is 0
      msg.send "You have not set any todos today or you have finished all of them."
    else
      for done in response.results
        if done.is_goal is true
          msg.send "#{done.raw_text}"

module.exports = (robot, room, name, env, adapter, done = ->) ->
  robot.respond /idonethis: (.*)$/i, (msg) ->
    robot.brain.set("idonethis_api_key_#{msg.message.user.name}", msg.match[1])
    msg.send "You should be good to add Idonethis dones and goals now!"

  robot.hear /^donetest: (.*)$/i, (msg) ->
    primary_message_split = msg.match[1].split " "
    if (primary_message_split[0] != "jira")
      msg.send "no jira |"
      return 0
    else
      jira_issue = primary_message_split[1]
      data = JSON.stringify({
        jira_issue : jira_issue,
      })
      robot.http("http://www.conquerthechaos.co.uk/hubot/jira.php?data=" + data).header('Content-Type', 'application/json').post(data) (err, res, body) ->
        msg.send body
        console.log(body)
      return 0

  robot.hear /^done: (.*)$/i, (msg) ->
    api_key = robot.brain.get("idonethis_api_key_#{msg.message.user.name}")
    task = msg.match[1]
    current_flow = msg.message.user.flow

    #if(current_flow == member_success_flow_id)
    #    team = "member-success"
    #else if (current_flow == tech_dev_flow_id)
    #    team = "tech-dev"
    #else if(current_flow == project_300_flow_id)
    #	team = "project-300"
    #else
    #  msg.send "Please use 'done: ' in Flows connected to iDoneThis (Tech. & Dev., Member Success, Project 300), Flow id " + current_flow + " not yet set"
    #  return 0

    #teams_parsed = JSON.parse teams
    find = '=>';
    re = new RegExp(find, 'g');
    teams_parsed = teams.replace(re, ":");
    teams_parsed = JSON.parse teams_parsed
    if(teams_parsed.hasOwnProperty(current_flow))
      team = teams_parsed[current_flow]
    else
      msg.send "Please use 'done: ' in Flows connected to iDoneThis (Tech. & Dev., Member Success, Project 300), Flow id " + current_flow + " not yet set "
      return 0
    if api_key and task isnt 'list'
      primary_message_split = msg.match[1].split " "
      if (primary_message_split[0].toLowerCase() == "jira")
        jira_issue = primary_message_split[1]
        data = JSON.stringify({
          jira_issue : jira_issue,
        })
        robot.http("http://www.conquerthechaos.co.uk/hubot/jira.php?data=" + data).header('Content-Type', 'application/json').post(data) (err, res, body) ->
          if(body == "no")
            msg.send "Jira issue '" + jira_issue.toUpperCase() + "' not found"
            return 0
          else
            task = "completed " + jira_issue.toUpperCase() + ": " + body + " - https://entrepreneurscircle.atlassian.net/browse/" + jira_issue
            params = {
                team: team,
                raw_text: task,
                jira: "yes"
              }
            #msg.send "Jira issue '" + jira_issue.toUpperCase() + "' : " + body + " set as completed in jira"
            #addDone msg, api_key, params
      else
        jira_issue = primary_message_split[0]
        data = JSON.stringify({
          jira_issue : jira_issue,
        })
        robot.http("http://www.conquerthechaos.co.uk/hubot/jira.php?data=" + data).header('Content-Type', 'application/json').post(data) (err, res, body) ->
          if(body == "no")
            params = {
              team: team,
              raw_text: task,
              jira: "no"
            }
            #addDone msg, api_key, params
          else
            task = "completed " + jira_issue.toUpperCase() + ": " + body + " - https://entrepreneurscircle.atlassian.net/browse/" + jira_issue
            params = {
                team: team,
                raw_text: task,
                jira: "yes"
              }
            #msg.send "Jira issue '" + jira_issue.toUpperCase() + "' : " + body + " set as completed in jira"
            #addDone msg, api_key, params
    else if api_key and task is 'list'
      getOwner msg, api_key, (data) ->
        if data.success
          getDones msg, api_key, data.owner
    else
      msg.send "I need your Idonethis API Token (go to https://idonethis.com/api/token/ to get it). Send it to me in a 1-1 like this: 'idonethis: 0a14bae..'"

  robot.hear /^do: (.*)$/i, (msg) ->
    user_json = JSON.stringify msg.message.user
    data = JSON.stringify({
      user_json : msg.message.user,
      msg : encodeURIComponent(msg.match[1])
    })
    robot.http("http://www.conquerthechaos.co.uk/hubot/todo.php?data=" + data).header('Content-Type', 'application/json').post(data) (err, res, body) ->
      msg.send body
      #console.log(body)
      return 0

  robot.hear /^todo: (.*)$/i, (msg) ->
    api_key = robot.brain.get("idonethis_api_key_#{msg.message.user.name}")
    todo = msg.match[1]

    current_flow = msg.message.user.flow
    find = '=>';
    re = new RegExp(find, 'g');
    teams_parsed = teams.replace(re, ":");
    teams_parsed = JSON.parse teams_parsed
    if(teams_parsed.hasOwnProperty(current_flow))
      team = teams_parsed[current_flow]
    else
      msg.send "Please use 'todo: ' in Flows connected to iDoneThis (Tech. & Dev., Member Success, Project 300), Flow id " + current_flow + " not yet set "
      return 0

    if api_key and todo isnt 'list'
      params = {
          team: team,
          raw_text: todo
        }
      addGoal msg, api_key, params
    else if api_key and todo is 'list'
      getOwner msg, api_key, (data) ->
        if data.success
          getGoals msg, api_key, data.owner

    else
      msg.send "I need your Idonethis API Token (go to https://idonethis.com/api/token/ to get it). Send it to me in a 1-1 like this: 'idonethis: 0a14bae..'"
