<?php

	include_once("Infusionsoft/isdk.php");

	$isdk = new iSDK();

	$apps = array(
		"ec" => array(
			"name" => "m148",
			"key"  => "d6ca5dfe82fbbe53c330280e5a75fff9"
		),
		"wg196" => array(
			"name" => "wg196",
			"key"  => "316bfb37d25027c67e20179444c42b84"
		),
		"hw232" => array(
			"name" => "hw232",
			"key"  => "724eba7382f4ac3def9c0c04b5db19f9"
		),
		"liz" => array(
			"name" => "bl110",
			"key"  => "ca18a9b2d03c8afda415c1d3754513d3"
		),
		"calloway" => array(
			"name" => "callowaygreen",
			"key"  => "6cfd7961ffadcb98711c8a434ae162d6"
		),
		"exela" => array(
			"name" => "exela",
			"key"  => "42b79372e4cea4fe3a8b11f235c0bfc9"
		),
		"massive" => array(
			"name" => "iwz87730",
			"key"  => "8dbc897316b34a270d6a2712ce632bf8"
		),
		"super" => array(
			"name" => "kw119",
			"key"  => "8655390eee9e9de69a463ba0933986deb2875cc3d41b5cac70075c9b964ca866"
		),
		"adi" => array(
			"name" => "kf101",
			"key"  => "ffcc48e4958722be5df0868cfa70b729"
		),
		"essential" => array(
			"name" => "mrb91054",
			"key"  => "c797daf9191f1aacf172e341f841e206"
		),
		"money" => array(
			"name" => "xp159",
			"key"  => "41e79f0b5b153911b46d254330bae0f1"
		),
		"stratique" => array(
			"name" => "ywx89342",
			"key"  => "70c72a51d837f49c05fb968620b481b5"
		),
		"forward" => array(
			"name" => "zv941",
			"key"  => "cf69dc1d745dcd645794f81401a616380adcc2cb9f745fbf76b9c0c13661749b"
		),
		"sbo" => array(
			"name" => "zy123",
			"key"  => "0358afd0dad444bc50ff7351ec66cad7b1f49dd082896050ac9efe8d9f9d3a8e"
		),
		"w10" => array(
			"name" => "dy158",
			"key"  => "6439dde6f45a4e3618bc49f89534f830582c49106bfed03c919fcabbdc5efd2a"
		),
		"bugfinders" => array(
			"name" => "na187",
			"key"  => "bd534d75fc2eb21cc4093c9771ec573a"
		),
		"linthwaite" => array(
			"name" => "gn187",
			"key"  => "5fa3d5ec68409110445ac61b6978f3ca48a9e387877bf54fd7583f7165116b93"
		),
		"forward2" => array(
			"name" => "ol192",
			"key"  => "ec84b105bcacb785dedd1700f99215d380c9d1eec734862b8c43f9bbf361834f"
		)
	);	

	$instructions = "
	    ______________________________\n
		Infusionsoft commands:\n\n
		with <APP> get contacts <FIELD> like '<search term>'\n
		with <APP> get tags for contact <Contact.Id>\n
		with <APP> get tag <Tag.Id>\n
		with <APP> get url for contact <Contact.Id>\n
		with <APP> get url for product <Product.Id>\n
		with <APP> get url for tag <Tag.Id>\n
		with <APP> get custom fields\n
		with <APP> get tags like '<search term>'\n
		with <APP> get product <Product.Id>\n
		with <APP> get tag like ‘<search term>’\n
		with <APP> get invoice <Invoice.Id>\n
		with <APP> get recent orders for contact <Contact.Id>\n
		with <APP> set contact <Contact.Id> <FIELD> to '<field value>'\n
		with <APP> create tag <TAG NAME>\n
		with <APP> apply tag <Tag.Id> to contact <Contact.Id>\n
		with <APP> remove tag <Tag.Id> from contact <Contact.Id>\n
		___________________________________________________________
	";

	$data     = explode(" ", json_decode($_REQUEST['data'])->data);

	if($data[0] == "help"){

		print $instructions;
		die();

	}else if($data[0] == "apps"){

		$output = "App names:\n";

		foreach($apps as $key => $value){
			$output .= $key . "\n";
		}

		print $output;
		die();

	}

	$app_name = strtolower($data[0]);

	if(!isset($apps[$app_name])){

		print "App not found! $app_name";
		die();

	}

	$isdk->cfgCon($apps[$app_name]['name'], $apps[$app_name]['key']);

	function get_custom_fields(){
		global $isdk;
		global $app_name;

		$returnFields  = array("Name", "Label");
		$query         = array("FormId" => -1);
		$custom_fields = $isdk->dsQuery("DataFormField",1000,0,$query,$returnFields);	

		return $custom_fields;	
	}

	function return_contact($search_by, $search_value){

		global $isdk;
		global $app_name;

		$returnFields = array("Id", "FirstName", "LastName", "Email");
		$query        = array($search_by => $search_value);
		$contacts     = $isdk->dsQuery("Contact",50,0,$query,$returnFields);

		if($contacts){

			return "Contact: " . $contacts[0]['FirstName'] . " " . $contacts[0]['LastName'] . " tags: \n";

		}else{

			return false;

		}

	}

	// function tag_name($tag_id){
	// 	global $isdk;

	// 	$returnFields = array("Id", "GroupName");
	// 	$query        = array('Id' => $tag_id);
	// 	$tags         = $isdk->dsQuery("ContactGroup",10,0,$query,$returnFields);

	// 	if($tags){

	// 		return $tags[0]['GroupName'];

	// 	}else{

	// 		return "not found";

	// 	}

	// }

	function remove_tag($tag_id, $contact_id){
		global $isdk;

		$result = $isdk->grpRemove($contact_id, $tag_id);

		return $result;

	}	

	function apply_tag($tag_id, $contact_id){
		global $isdk;

		$result = $isdk->grpAssign($contact_id, $tag_id);

		return $result;

	}

	function create_tag($tag_name){
		global $isdk;

		$conDat = array('GroupName' => $tag_name);
		$tagId  = $isdk->dsAdd("ContactGroup", $conDat);

		return $tagId;
	}

	function update_contact($contact_id, $field, $value){
		global $isdk;

		$conDat = array($field => $value);
		$conID  = $isdk->updateCon($contact_id, $conDat);

		return $conID;

	}

	function get_recent_invoices($contact_id){
		global $isdk;

		$returnFields = array("Id", "ContactId", "DateCreated", "Description", "InvoiceTotal", "InvoiceType", "PayStatus", "ProductSold", "TotalDue", "TotalPaid");
		$query        = array('ContactId' => $contact_id);
		$invoice      = $isdk->dsQuery("Invoice",100,0,$query,$returnFields, "DateCreated", false);

		if($invoice){

			return $invoice;

		}else{

			return "not found";

		}	
	}

	function get_invoice_by_id($id){
		global $isdk;

		$returnFields = array("Id", "ContactId", "DateCreated", "Description", "InvoiceTotal", "InvoiceType", "PayStatus", "ProductSold", "TotalDue", "TotalPaid");
		$query        = array('Id' => $id);
		$invoice      = $isdk->dsQuery("Invoice",100,0,$query,$returnFields);

		if($invoice){

			return $invoice;

		}else{

			return "not found";

		}
	}

	function get_product_by_id($id){

		global $isdk;

		$returnFields = array("Id", "Description", "ProductName", "ProductPrice", "Sku", "Status", "ShortDescription");
		$query        = array('Id' => $id);
		$product      = $isdk->dsQuery("Product",100,0,$query,$returnFields);

		if($product){

			return $product;

		}else{

			return "not found";

		}		

	}

	function get_tag_by_name($name){

		global $isdk;

		$returnFields = array("Id", "GroupName", "GroupCategoryId");
		$query        = array('GroupName' => "%" . $name . "%");
		$tags         = $isdk->dsQuery("ContactGroup",1000,0,$query,$returnFields);

		if($tags){

			return $tags;

		}else{

			return "not found";

		}


	}

	function tag_name($tag_id){
		global $isdk;

		$returnFields = array("Id", "GroupName", "GroupCategoryId");
		$query        = array('Id' => $tag_id);
		$tags         = $isdk->dsQuery("ContactGroup",10,0,$query,$returnFields);

		if($tags){

			return array("name" => $tags[0]['GroupName'], "cat" => $tags[0]['GroupCategoryId']);

		}else{

			return "not found";

		}

	}

	function tag_category($tag_id){

		global $isdk;

		$returnFields = array("Id", "GroupName", "GroupCategoryId");
		$query        = array('Id' => $tag_id);
		$tag         = $isdk->dsQuery("ContactGroup",10,0,$query,$returnFields);

		$returnFields = array("Id", "CategoryName");
		$query        = array('Id' => $tag[0]['GroupCategoryId']);
		$category     = $isdk->dsQuery("ContactGroupCategory",10,0,$query,$returnFields);

		return $category[0]['CategoryName'];

	}

	function return_contact_tag_names($contact_id){

		global $isdk;

		$returnFields = array("ContactGroup", "DateCreated", "GroupId");
		$query        = array('ContactId' => $contact_id);
		$tags         = $isdk->dsQuery("ContactGroupAssign",1000,0,$query,$returnFields);

		$data = "";

		foreach($tags as $tag){

			$date = DateTime::createFromFormat('YmdH:i:s', str_replace("T","",$tag['DateCreated']))->format('M j, Y H:i');
			$data .= $tag['GroupId'] . " - " . tag_category($tag['GroupId']) ." | " . $tag['ContactGroup'] . " : applied " . $date . "\n";

		}

		return $data;

	}

?>