<?php

	ini_set('display_errors',1);
	ini_set('display_startup_errors',1);
	error_reporting(-1);

	function make_curl_request($extra_url, $postFields = null){

		$jira_url  = "https://entrepreneurscircle.atlassian.net";
		$jira_user = "peterdalydickson";
		$jira_pass = "Chief&Engineer84310";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$jira_url . $extra_url);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		
		// curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		// curl_setopt($ch, CURLOPT_USERPWD, "$jira_user:$jira_pass");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Basic ' . base64_encode("$jira_user:$jira_pass"), 'Content-Type: application/json') );

		if($postFields){

			//echo "postfields: $postFields";

			curl_setopt($ch, CURLOPT_POST,1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,$postFields);

		}

		$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
		$result      = curl_exec ($ch);

		//echo 'Curl error: ' . curl_error($ch);

		curl_close ($ch);

		return $result;

	}

	$post_data = array(
		"jql" => "assignee = andrei AND status != Done"
	);	

	$issues = make_curl_request("/rest/api/2/search", json_encode($post_data));
	$issues = json_decode($issues);

	if($issues->total == 0){

		echo "No available issues found.";
		exit();

	}

	$issues_array = array();

	foreach($issues->issues as $issue){

		// echo "<pre>";
		// 	print_r($issue);
		// echo "</pre>";

		$key      = $issue->key;
		$priority = $issue->fields->priority->id;
		$p_name   = $issue->fields->priority->name;
		$desc     = substr($issue->fields->summary, 0, 100);
		$proj     = explode("-", $key);

		$desc = "$desc | https://entrepreneurscircle.atlassian.net/browse/" . $key . " | $p_name ";

		$issue_details = array(
			"desc"     => $issue->fields->summary,
			"key"      => $key,
			"priority" => $p_name
		);

		$issues_array[$priority][$proj[0]][$key] = $issue_details;

	}

	ksort($issues_array);

	foreach($issues_array as $priority => $projects){

		foreach ($projects as $project => $issues) {
			
			uksort($issues, 'strnatcmp');

			foreach($issues as $issue){

				// echo "<pre>";
				// 	print_r($issue);
				// echo "</pre>";

				$output = $issue['key'] . " | ";
				$count  = strlen($output);
				$rest   = 90 - $count;

				if(strlen($issue['desc']) <= $rest){

					$output .= $issue['desc'];

				}else{

					$output .= substr($issue['desc'], 0, $rest) . "...";

				}

				$output .= "\nhttps://entrepreneurscircle.atlassian.net/browse/" . $issue['key'] . " | " . $issue['priority'] . "\n\n";

				echo $output;

			}

		}

	}

	// echo "<pre>";
	// 	print_r($issues_array);
	// echo "</pre>";

	// foreach($issues_array as $key => $issues){

	// 	foreach($issues as $issue){

	// 		echo $issue . "<br>";

	// 	}

	// }

	// echo "<pre>";
	// 	print_r($issues_array);
	// echo "</pre>";

	// echo "<pre>";
	// 	print_r($issues);
	// echo "</pre>";

?>