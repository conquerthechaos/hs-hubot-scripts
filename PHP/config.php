<?php

	include_once("Infusionsoft/isdk.php");

	$isdk = new iSDK();

	$apps = array(
		"n5" => array(
			"name" => "m148",
			"key"  => "d6ca5dfe82fbbe53c330280e5a75fff9"
		),
		"liz" => array(
			"name" => "bl110",
			"key"  => "ca18a9b2d03c8afda415c1d3754513d3"
		),
		"calloway" => array(
			"name" => "callowaygreen",
			"key"  => "6cfd7961ffadcb98711c8a434ae162d6"
		),
		"exela" => array(
			"name" => "exela",
			"key"  => "42b79372e4cea4fe3a8b11f235c0bfc9"
		),
		"massive" => array(
			"name" => "iwz87730",
			"key"  => "8dbc897316b34a270d6a2712ce632bf8"
		),
		"super" => array(
			"name" => "kw119",
			"key"  => "8655390eee9e9de69a463ba0933986deb2875cc3d41b5cac70075c9b964ca866"
		),
		"adi" => array(
			"name" => "kf101",
			"key"  => "ffcc48e4958722be5df0868cfa70b729"
		),
		"essential" => array(
			"name" => "mrb91054",
			"key"  => "c797daf9191f1aacf172e341f841e206"
		),
		"money" => array(
			"name" => "xp159",
			"key"  => "41e79f0b5b153911b46d254330bae0f1"
		),
		"stratique" => array(
			"name" => "ywx89342",
			"key"  => "70c72a51d837f49c05fb968620b481b5"
		),
		"forward" => array(
			"name" => "zv941",
			"key"  => "cf69dc1d745dcd645794f81401a616380adcc2cb9f745fbf76b9c0c13661749b"
		),
		"sbo" => array(
			"name" => "zy123",
			"key"  => "0358afd0dad444bc50ff7351ec66cad7b1f49dd082896050ac9efe8d9f9d3a8e"
		),
		"w10" => array(
			"name" => "dy158",
			"key"  => "6439dde6f45a4e3618bc49f89534f830582c49106bfed03c919fcabbdc5efd2a"
		),
		"bugfinders" => array(
			"name" => "na187",
			"key"  => "bd534d75fc2eb21cc4093c9771ec573a"
		),
		"linthwaite" => array(
			"name" => "gn187",
			"key"  => "5fa3d5ec68409110445ac61b6978f3ca48a9e387877bf54fd7583f7165116b93"
		),
		"forward2" => array(
			"name" => "ol192",
			"key"  => "ec84b105bcacb785dedd1700f99215d380c9d1eec734862b8c43f9bbf361834f"
		)
	);	

	$data       = json_decode($_REQUEST['data']);
	$app_name   = strtolower($data->app_name);

	if(!isset($apps[$app_name])){

		print "App not found!";
		die();

	}

	$isdk->cfgCon($apps[$app_name]['name'], $apps[$app_name]['key']);

?>