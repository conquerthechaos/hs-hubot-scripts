<?php

	include_once('config.php');

	$tags         = $data->tags;
	$search_by    = $data->search_by;
	$search_value = $data->search_value;

	function return_contact($search_by, $search_value){

		global $isdk;
		global $app_name;
		global $apps;

		$returnFields = array("Id", "FirstName", "LastName", "Email");

		if($app_name == "n5"){

			$returnFields[] = '_DDReferenceNumber';
			$returnFields[] = '_PREMIERreferencenum';
			$returnFields[] = '_TrainingAcademyReferenceNumber';

		}

		$query        = array($search_by => $search_value);
		$contacts     = $isdk->dsQuery("Contact",50,0,$query,$returnFields);

		if($contacts){

			if(count($contacts) == 1){
				$out = "Contact found:\n";
			}else{
				$out = "Contacts found (" . count($contacts) . ") :\n";
			}
			
			foreach($contacts as $contact){

				$out .= "Id : " . $contact['Id'] . "\n";
				$out .= "FirstName : " . $contact['FirstName'] . "\n";
				$out .= "LastName : " . $contact['LastName'] . "\n";
				$out .= "Email : " . $contact['Email'] . "\n";

				if($app_name == "n5"){

					$out .= "DD ref: " . $contact['_DDReferenceNumber'] . "\n";
					$out .= "Premier ref: " . $contact['_PREMIERreferencenum'] . "\n";
					$out .= "Training ref: " . $contact['_TrainingAcademyReferenceNumber'] . "\n";

				}

				$out .= "url : https://" . $apps[$app_name]['name'] . ".infusionsoft.com/Contact/manageContact.jsp?view=edit&ID=" . $contact['Id'] . "\n";
				$out .= "\n--------------------------------------\n";

			}

			print $out;

		}else{

			return "Contact not found searching '$search_value' in '$search_by'";

		}

	}

	print return_contact($search_by, $search_value);

?>