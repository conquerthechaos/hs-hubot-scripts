<?php

	ini_set('display_errors',1);
	ini_set('display_startup_errors',1);
	error_reporting(-1);

	//$_REQUEST['data'] = '{"user_json":{"id":"90968","name":"Andrei","flow":"679cdbf6-e9ae-49d0-b000-811f00f9b5ce","message":623933},"msg":"what"}';

	$data = json_decode($_REQUEST['data']);

	file_put_contents("todo_call.txt", "--------------------------------------\n" . "Post was made at: " . date("Y-m-d H:i") . "\n" . $_REQUEST['data'] , FILE_APPEND);

	function make_curl_request($extra_url, $postFields = null){

		$jira_url  = "https://entrepreneurscircle.atlassian.net";
		$jira_user = "peterdalydickson";
		$jira_pass = "Chief&Engineer84310";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$jira_url . $extra_url);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		
		// curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		// curl_setopt($ch, CURLOPT_USERPWD, "$jira_user:$jira_pass");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Basic ' . base64_encode("$jira_user:$jira_pass"), 'Content-Type: application/json') );

		if($postFields){

			//echo "postfields: $postFields";

			curl_setopt($ch, CURLOPT_POST,1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,$postFields);

		}

		$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
		$result      = curl_exec ($ch);

		//echo 'Curl error: ' . curl_error($ch);

		curl_close ($ch);

		return $result;

	}

	$user_list = array(
		"andrei"     => "andrei",
		"peter"      => "peterdalydickson",
		"nathan"     => "nathan",
		"geover"     => "geover",
		"vik"        => "vik",
		"pieter"     => "pieter",
		"unassigned" => "unassigned"
	);

	if(strtolower($data->msg) == "help"){

		echo "Call should be: `do: projectkey [ defaults to task | story | bug ] [ defaults to unassigned | me | firstname ] issue summary`";
		exit();

	}else if(strtolower($data->msg) == "what"){

		$post_data = array(
			"jql" => "assignee = " . $user_list[strtolower($data->user_json->name)] . " AND status != Done"
		);	

		$issues = make_curl_request("/rest/api/2/search", json_encode($post_data));
		$issues = json_decode($issues);

		if($issues->total == 0){

			echo "No available issues found.";
			exit();

		}

		$issues_array = array();

		// foreach($issues->issues as $issue){

		// 	// echo "<pre>";
		// 	// 	print_r($issue);
		// 	// echo "</pre>";

		// 	$key      = $issue->key;
		// 	$priority = $issue->fields->priority->id;
		// 	$p_name   = $issue->fields->priority->name;
		// 	$desc     = substr($issue->fields->summary, 0, 100);

		// 	$desc = "$desc | https://entrepreneurscircle.atlassian.net/browse/" . $key . " | $p_name ";

		// 	$issues_array[$priority][] = $desc;

		// }

		// ksort($issues_array);

		// foreach($issues_array as $key => $issues){

		// 	foreach($issues as $issue){

		// 		echo $issue . "\n";

		// 	}

		// }

		$issues_array = array();

		foreach($issues->issues as $issue){

			// echo "<pre>";
			// 	print_r($issue);
			// echo "</pre>";

			$key      = $issue->key;
			$priority = $issue->fields->priority->id;
			$p_name   = $issue->fields->priority->name;
			$desc     = substr($issue->fields->summary, 0, 100);
			$proj     = explode("-", $key);

			$desc = "$desc | https://entrepreneurscircle.atlassian.net/browse/" . $key . " | $p_name ";

			$issue_details = array(
				"desc"     => $issue->fields->summary,
				"key"      => $key,
				"priority" => $p_name
			);

			$issues_array[$priority][$proj[0]][$key] = $issue_details;

		}

		ksort($issues_array);

		foreach($issues_array as $priority => $projects){

			foreach ($projects as $project => $issues) {
				
				uksort($issues, 'strnatcmp');

				foreach($issues as $issue){

					// echo "<pre>";
					// 	print_r($issue);
					// echo "</pre>";

					$output = $issue['key'] . " | ";
					$count  = strlen($output);
					$rest   = 90 - $count;

					if(strlen($issue['desc']) <= $rest){

						$output .= $issue['desc'];

					}else{

						$output .= substr($issue['desc'], 0, $rest) . "...";

					}

					$output .= "\nhttps://entrepreneurscircle.atlassian.net/browse/" . $issue['key'] . " | " . $issue['priority'] . "\n\n";

					echo $output;

				}

			}

		}
	
		exit();		

	}

	$message = urldecode($data->msg);
	$message = explode(" ", $message);

	if(count($message) < 2){

		$jira_issue = $message[0];

		$issue = make_curl_request("/rest/api/2/issue/$jira_issue/");
		$issue = json_decode($issue);

		if(isset($issue->errorMessages)){

			echo "Issue '" . $message[0] . "' not found.";
			exit();

		}else{

			$issue_message = $issue->fields->summary;
			$post_fields = array(
				"transition" => array(
					"id" => 31
				)
			);

			$change_status_response = make_curl_request("/rest/api/2/issue/$jira_issue/transitions?expand=transitions.fields", json_encode($post_fields));
			//$change_status_response = make_curl_request("/rest/api/2/issue/HTII-2/transitions?expand=transitions.fields");

			//echo $issue_message;
			echo "Issue '" . $message[0] . "' moved to 'In Progress'.";
			exit();

		}

	}

	$story        = $message[0];
	$issue_type   = "Task";
	$had_type_arg = false;

	if(strtolower($message[1]) == "story"){
		$issue_type = "Story";
		$had_type_arg = true;
	}else if(strtolower($message[1]) == "bug"){
		$issue_type = "Bug";
		$had_type_arg = true;
	}else if(strtolower($message[1]) == "task"){
		$issue_type == "Task";
		$had_type_arg = true;
	}


	$start_from = 1;

	if($had_type_arg){
		$start_from = 2;
	}

	$assigned_to = strtolower($data->user_json->name);
	$assigned_to = "unassigned";

	if(strtolower($message[$start_from]) == "assign"){

		$assigned_to = $message[$start_from+1];
		$start_from+=2;

		if(strtolower($assigned_to) == "me"){

			$assigned_to = strtolower($data->user_json->name);

		}

	}

	$issue_contents = "";

	for ($i=$start_from; $i < count($message); $i++) { 
		$issue_contents .= $message[$i] . " ";
	}


	$story   = strtoupper($story);
	$project = make_curl_request("/rest/api/2/project/$story/");
	$project = json_decode($project);

	if(isset($project->errorMessages)){
		echo "Project not found";
		exit();
	}

	if(!isset($user_list[$assigned_to])){

		echo "User `$assigned_to` not found!";
		exit();

	}

	if(!isset($user_list[strtolower($data->user_json->name)])){

		echo "Your user `" . $data->user_json->name . "` is not setup in the do: call";
		exit();

	}

	$post_data = array(

		"fields" => array(
			"project" => array(
				"key" => $story
			),
			"issuetype" => array(
				"name" => $issue_type
			),
			"reporter" => array(
				"name" => $user_list[strtolower($data->user_json->name)]
			),
			"summary"     => $issue_contents,
			//"description" => $issue_contents
		)

	);

	if($user_list[$assigned_to] != "unassigned"){

		$post_data["fields"]["assignee"] = array(
			"name" => $user_list[$assigned_to]
		);

	}

	$issue = make_curl_request("/rest/api/2/issue", json_encode($post_data));

	$issue = json_decode($issue);

	echo "Created issue " . $issue->key . " for @" . ucwords($assigned_to) . " - https://entrepreneurscircle.atlassian.net/browse/" . $issue->key;

	//echo "Created issue " . $issue->key . " | Link: " . $issue->self;

	//do: htii task assign andrei Do you think you can get this to work?!
	/*
	do: story assign andrei This is the issue 
 PeterPeter at 11:36
if story or bug is missing, assume the issue type is task
htti bug assign andrei This is the issue 
	*/

?>