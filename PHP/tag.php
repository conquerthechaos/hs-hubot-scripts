<?php

	include_once('config.php');

	$tags     = $data->tags;

	//143,90

	function tag_name($tag_id){
		global $isdk;

		$returnFields = array("Id", "GroupName", "GroupCategoryId");
		$query        = array('Id' => $tag_id);
		$tags         = $isdk->dsQuery("ContactGroup",10,0,$query,$returnFields);

		if($tags){

			return array("name" => $tags[0]['GroupName'], "cat" => $tags[0]['GroupCategoryId']);

		}else{

			return "not found";

		}

	}

	function tag_category($cat_id){

		global $isdk;

		$returnFields = array("Id", "CategoryName");
		$query        = array('Id' => $cat_id);
		$tags         = $isdk->dsQuery("ContactGroupCategory",10,0,$query,$returnFields);

		return $tags[0]['CategoryName'];

	}

	$tags = explode(",", $tags);

	$output = "";

	foreach($tags as $tag){

		$data =  tag_name($tag);

		$output .= "$tag - " . tag_category($data['cat']) . " | " . $data['name'] . "\n";

	}

	//print $output;
?>