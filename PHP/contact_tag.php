<?php

	include_once('config.php');

	$contact_id = $data->contact_id;

	//143,90

	function return_contact($search_by, $search_value){

		global $isdk;
		global $app_name;

		$returnFields = array("Id", "FirstName", "LastName", "Email");
		$query        = array($search_by => $search_value);
		$contacts     = $isdk->dsQuery("Contact",50,0,$query,$returnFields);

		if($contacts){

			return "Contact: " . $contacts[0]['FirstName'] . " " . $contacts[0]['LastName'] . " tags: \n";

		}else{

			return false;

		}

	}

	function tag_name($tag_id){
		global $isdk;

		$returnFields = array("Id", "GroupName");
		$query        = array('Id' => $tag_id);
		$tags         = $isdk->dsQuery("ContactGroup",10,0,$query,$returnFields);

		if($tags){

			return $tags[0]['GroupName'];

		}else{

			return "not found";

		}

	}

	function tag_category($tag_id){

		global $isdk;

		$returnFields = array("Id", "GroupName", "GroupCategoryId");
		$query        = array('Id' => $tag_id);
		$tag         = $isdk->dsQuery("ContactGroup",10,0,$query,$returnFields);

		$returnFields = array("Id", "CategoryName");
		$query        = array('Id' => $tag[0]['GroupCategoryId']);
		$category     = $isdk->dsQuery("ContactGroupCategory",10,0,$query,$returnFields);

		return $category[0]['CategoryName'];

	}

	function return_contact_tag_names($contact_id){

		global $isdk;

		$returnFields = array("ContactGroup", "DateCreated", "GroupId");
		$query        = array('ContactId' => $contact_id);
		$tags         = $isdk->dsQuery("ContactGroupAssign",1000,0,$query,$returnFields);

		$data = "";

		foreach($tags as $tag){

			$date = DateTime::createFromFormat('YmdH:i:s', str_replace("T","",$tag['DateCreated']))->format('M j, Y H:i');
			$data .= $tag['GroupId'] . " - " . tag_category($tag['GroupId']) ." | " . $tag['ContactGroup'] . " : applied " . $date . "\n";

		}

		return $data;

	}

	$contact_data = return_contact("Id", $contact_id);

	if($contact_data){

		$extra = return_contact_tag_names($contact_id);
		print $contact_data . $extra;

	}else{

		print "Contact not found";

	}
?>