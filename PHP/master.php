<?php
	include_once('config_new.php');

	if( isset($data[1]) && $data[1] == "remove"){

		if( isset($data[2]) && $data[2] == "tag" && $data[4] == "from" && $data[5] == "contact" && count($data) >= 7){

			$result = remove_tag($data[3], $data[6]);

			if($result == 1){
				print "Tag removed";
			}else{
				print "Error: " . $result;
			}

		}else{
			print "invalid command";
		}

	}else if( isset($data[1]) && $data[1] == "apply"){

		if( isset($data[2]) && $data[2] == "tag" && $data[4] == "to" && $data[5] == "contact" && count($data) >= 7){

			$result = apply_tag($data[3], $data[6]);

			if($result == 1){
				print "Tag applied";
			}else{
				print "Error: " . $result;
			}

		}else{
			print "invalid command";
		}

	}else if( isset($data[1]) && $data[1] == "create"){

		if( isset($data[2]) && $data[2] == "tag"){
		
			$tag_name = "";
			for ($i=3; $i < count($data); $i++) { 
				$temp = str_replace("'", "", $data[$i]);
				if($i > 3){
					$tag_name .= " ";
				}
				$tag_name .= $temp;

			}

			$result = create_tag($tag_name);

			if(is_int($result)){
				print "Tag created..tag id: " . $result;
			}else{
				print "Error: " . $result;
			}

		}else{

			print "invalid command";

		}

	}else if( isset($data[1]) && $data[1] == "set"){

		if( isset($data[2]) && $data[2] == "contact" && count($data) >= 7){

			$value = "";
			for ($i=7; $i < count($data); $i++) { 
				$temp = str_replace("'", "", $data[$i]);
				if($i > 7){
					$value .= " ";
				}
				$value .= $temp;

			}

			$result = update_contact($data[3], $data[4], $value);

			if(is_int($result)){
				print "Contact updated";
			}else{
				print "Error: " . $result;
			}

		}else{
			print "invalid command";
		}

	}else if( isset($data[1]) && $data[1] == "get"){

		if( isset($data[2]) && $data[2] == "contacts" && count($data) == 6){

			$field        = $data[3];
			$search_term  = $data[5];
			$returnFields = array("Id", "FirstName", "LastName", "Email");
			$search_by    = array($field => $search_term);

			$contacts     = $isdk->dsQuery("Contact",1000,0,$search_by,$returnFields);

			if(is_array($contacts)){

				if(count($contacts) == 1){
					$out = "Contact found:\n";
				}else{
					$out = "Contacts found (" . count($contacts) . ") :\n";
				}
				
				foreach($contacts as $contact){

					$out .= "Id : " . $contact['Id'] . "\n";
					$out .= "FirstName : " . $contact['FirstName'] . "\n";
					$out .= "LastName : " . $contact['LastName'] . "\n";
					$out .= "Email : " . $contact['Email'] . "\n";

					if($app_name == "n5"){

						$out .= "DD ref: " . $contact['_DDReferenceNumber'] . "\n";
						$out .= "Premier ref: " . $contact['_PREMIERreferencenum'] . "\n";
						$out .= "Training ref: " . $contact['_TrainingAcademyReferenceNumber'] . "\n";

					}

					$out .= "url : https://" . $apps[$app_name]['name'] . ".infusionsoft.com/Contact/manageContact.jsp?view=edit&ID=" . $contact['Id'] . "\n";
					$out .= "\n--------------------------------------\n";

				}

				print $out;

			}else{

				print "Contact not found searching '$search_term' in '$field'";

			}

		}else if( isset($data[2]) && $data[2] == "tags" && count($data) == 6 && $data[3] == "for" && $data[4] == "contact"){

			$contact_id   = $data[5];
			$contact_data = return_contact("Id", $contact_id);

			if($contact_data){

				$extra = return_contact_tag_names($contact_id);
				print $contact_data . $extra;

			}else{

				print "Contact not found";

			}

		}else if( isset($data[2]) && $data[2] == "tag" && count($data) == 4 ){

			$tags = explode(",", $data[3]);

			$output = "";

			foreach($tags as $tag){

				$data =  tag_name($tag);

				$output .= tag_category($tag) . " | " . $data['name'] . "\n";

			}

			print $output;

		}else if( isset($data[2]) && ($data[2] == "tags" || $data[2] == "tag") && $data[3] == "like" ){

			$search_term = "";
			for ($i=4; $i < count($data); $i++) { 
				$temp = str_replace("'", "", $data[$i]);
				if($i > 4){
					$search_term .= " ";
				}
				$search_term .= $temp;

			}

			$tags = get_tag_by_name($search_term);

			if($tags == "not found"){
				print "tag(s) not found";
			}else{
				$output = "";

				foreach($tags as $tag){
					$output .= "Tag id: " . $tag['Id'] . " | Tag name: " . $tag['GroupName'] . " | Tag category: " . tag_category($tag['Id']) . "\n";
				}

				print $output;
			}

		}else if(isset($data[2]) && $data[2] == "url" && count($data) == 6){

			if($data[4] == "contact"){
				print "https://" . $apps[$app_name]['name'] . ".infusionsoft.com/Contact/manageContact.jsp?view=edit&ID=" . $data[5];
			}else if($data[4] == "product"){	
				print "https://" . $apps[$app_name]['name'] . ".infusionsoft.com/app/product/manageProduct?productId=" . $data[5];
			}else if($data[4] == "tag"){
				print "https://" . $apps[$app_name]['name'] . ".infusionsoft.com/ContactGroup/manageContactGroup.jsp?view=edit&ID=" . $data[5];
			}else{	
				print "invalid command";
			}

		}else if(isset($data[2]) && $data[2] == "custom" && $data[3] == "fields" && count($data) == 4){

			$custom_fields = get_custom_fields();
			$return_string = "";
			foreach($custom_fields as $field){

				$return_string .= "Name: " . $field['Name'] . " | Label: " . $field['Label'] . "\n";

			}

			print $return_string;

		}else if( isset($data[2]) && $data[2] == "product" && count($data) == 4){
			$product = get_product_by_id($data[3]);

			if($product == "not found"){
				print "product not found";
			}else{
				$output  = "__________________________________\n";
				$output .= "Product name: " . $product[0]['ProductName'];
				$output .= "Description: " . $product[0]['Description'];
				$output .= "ShortDescription: " . $product[0]['ShortDescription'];
				$output .= "ProductPrice: " . $product[0]['ProductPrice'];
				$output .= "Status: " . $product[0]['Status'];
				$output .= "Sku: " . $product[0]['Sku'];
				$output .= "__________________________________\n";

				print $output;
			}

			//array("Id", "Description", "ProductName", "ProductPrice", "Sku", "Status", "ShortDescription");

		}else if(isset($data[2]) && $data[2] == "invoice" && count($data) == 4){
			$invoice = get_invoice_by_id($data[3]);
			if($invoice == "not found"){
				print "invoice not found";
			}else{

				$output  = "__________________________________\n";
				$output .= "Description: " . $invoice[0]['Description'];
				$output .= "InvoiceTotal: " . $invoice[0]['InvoiceTotal'];
				$output .= "InvoiceType: " . $invoice[0]['InvoiceType'];
				$output .= "TotalDue: " . $invoice[0]['TotalDue'];
				$output .= "TotalPaid: " . $invoice[0]['TotalPaid'];
				$output .= "PayStatus: " . $invoice[0]['PayStatus'];
				$output .= "ProductSold: " . $invoice[0]['ProductSold'];
				$output .= "ContactId: " . $invoice[0]['ContactId'];
				$date    = DateTime::createFromFormat('YmdH:i:s', str_replace("T","",$invoice[0]['DateCreated']))->format('M j, Y');
				$output .= "DateCreated: " . $date;
				$output .= "__________________________________\n";

				print $output;
				//$returnFields = array("Id", "ContactId", "DateCreated", "Description", "InvoiceTotal", "InvoiceType", "PayStatus", "ProductSold", "TotalDue", "TotalPaid"); 428

			}
		}else if(isset($data[2]) && $data[2] == "recent" && $data[3] == "orders" && $data[4] == "for" && $data[5] == "contact" && count($data) == 7){
		
			$invoices = get_recent_invoices($data[3]);
			if($invoices == "not found"){
				print "invoice not found";
			}else{

				$output = "";

				foreach($invoices as $invoice){

					$output .= "Description: " . $invoice['Description'] ."\n";
					$output .= "InvoiceTotal: " . $invoice['InvoiceTotal'] . "\n";
					$output .= "InvoiceType: " . $invoice['InvoiceType'] . "\n";
					$output .= "TotalDue: " . $invoice['TotalDue'] . "\n";
					$output .= "TotalPaid: " . $invoice['TotalPaid'] . "\n";
					$output .= "PayStatus: " . $invoice['PayStatus'] . "\n";
					$output .= "ProductSold: " . $invoice['ProductSold'] . "\n";
					$output .= "ContactId: " . $invoice['ContactId'] . "\n";
					$date    = $invoice['DateCreated'] . "\n";

					if(isset($invoice['DateCreated'])){
						$date    = DateTime::createFromFormat('YmdH:i:s', str_replace("T","",$invoice['DateCreated']))->format('M j, Y');
					}
					
					$output .= "DateCreated: " . $date ."\n";
					$output .= "__________________________________\n";					

				}

				print $output;
				//$returnFields = array("Id", "ContactId", "DateCreated", "Description", "InvoiceTotal", "InvoiceType", "PayStatus", "ProductSold", "TotalDue", "TotalPaid"); 428

			}

		}else{

			print "invalid command " . count($data);

		}

	}else{

		print "invalid command";

	}
?>