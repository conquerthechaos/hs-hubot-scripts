# Description
#   <description of the scripts functionality>
#
# Dependencies:
#   "<module name>": "<module version>"
#
# Configuration:
#   LIST_OF_ENV_VARS_TO_SET
#
# Commands:
#   hubot <trigger> - <what the respond trigger does>
#   <trigger> - <what the hear trigger does>
#
# URLS:
#   GET /path?param=<val> - <what the request does>
#
# Notes:
#   <optional notes required for the script>
#
# Author:
#   <github username of the original script author>

module.exports = (robot) ->
  robot.respond /create project (.*)/i, (msg) ->
    projectName = msg.match[1].replace(/\ /g, "_")
    if projectName.length > 32 || projectName.length < 1
      msg.reply "Project name must be between 1 and 32 char long"
      return
    apiToken = process.env.HUBOT_ROLLBAR_ACCESS_WRITE
    data = JSON.stringify({
      name : projectName,
      access_level: "standard"
    })
    robot.http("https://api.rollbar.com/api/1/projects?access_token=#{apiToken}").header('Content-Type', 'application/json').post(data) (err, res, body) ->
      body_obj = JSON.parse body
      if body_obj.err > 0
        msg.reply "Error creating project #{projectName}: #{body_obj.message}"
        return
      msg.reply "Project #{projectName} created: #{body}"

    
    