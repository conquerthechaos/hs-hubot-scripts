# Description
#   <description of the scripts functionality>
#
# Dependencies:
#   "<module name>": "<module version>"
#
# Configuration:
#   LIST_OF_ENV_VARS_TO_SET
#
# Commands:
#   hubot <trigger> - <what the respond trigger does>
#   <trigger> - <what the hear trigger does>
#
# URLS:
#   GET /path?param=<val> - <what the request does>
#
# Notes:
#   <optional notes required for the script>
#
# Author:
#   <github username of the original script author>

module.exports = (robot) ->
  robot.respond /with (.*)/i, (msg) ->
    data = JSON.stringify({
      data : msg.match[1]
    })
    robot.http("http://www.conquerthechaos.co.uk/hubot/master.php?data="+data).header('Content-Type', 'application/json').post(data) (err, res, body) ->
      #body_obj = JSON.parse body
      msg.reply body